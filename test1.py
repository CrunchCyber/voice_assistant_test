import deepspeech
import os
import sys
import pathlib
import pyaudio
import numpy as np
import time
import pyttsx3

DEEPSPEECH_MODEL = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ds07/deepspeech-0.7.4-models.pbmm')
DEEPSPEECH_SCORER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'ds07/deepspeech-0.7.4-models.scorer')

ds = deepspeech.Model(DEEPSPEECH_MODEL)
# ds.setBeamWidth(500)
ds.enableExternalScorer(DEEPSPEECH_SCORER)

stream = ds.createStream()

text_so_far = ''

e = pyttsx3.init()

def process_audio(in_data, frame_count, time_info, status):
    global text_so_far
    data16 = np.frombuffer(in_data, dtype=np.int16)
    stream.feedAudioContent(data16)
    text = stream.intermediateDecode()
        
    if text != text_so_far:
        t = text.split()
        if t[-1] == 'exit':
            e.say('good bye')
            e.runAndWait()
            sys.exit(0)
        elif t[-1] == 'james':
            e.say('how may i help you')
            e.runAndWait()
        text_so_far = text
    return (in_data, pyaudio.paContinue)

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK_SIZE = 1024

audio = pyaudio.PyAudio()
audio_stream = audio.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    frames_per_buffer=CHUNK_SIZE,
    stream_callback=process_audio
)

print('Please start speaking, when done press CTRL+C')
audio_stream.start_stream()

try:
    while audio_stream.is_active():
        time.sleep(0.1)
except KeyboardInterrupt:
    audio_stream.stop_stream()
    audio_stream.close()
    audio.terminate()
    print('Finished Recording')
    text = stream.finishStream()
    print('Final Text: {}'.format(text))